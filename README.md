# onesignal-demo
###1 Probar la aplicación utilizando cliente de Cordova

Clonar el repo, agregar platforms Andorid, agregar plugin de notificaciones, configurar keys, correrlo!

1. `git clone https://bitbucket.org/somospnt/onesignal-demo.git`
2. `cordova platform add android`
3. `cordova plugin add onesignal-cordova-plugin`
4. Modificar el archivo www\js\index.js y agergar la key de la aplicación de OneSignal y el id de la aplicación de google GCM.
5. `cordova run android`

###2 Enviar mensaje desde la consola de OneSignal

1. https://onesignal.com/apps
2. Seleccionar la aplicación correspondiente
3. Enviar un nuevo mensaje.